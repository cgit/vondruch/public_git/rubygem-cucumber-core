# Generated from cucumber-core-1.4.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name cucumber-core

Name: rubygem-%{gem_name}
Version: 1.4.0
Release: 1%{?dist}
Summary: Core library for the Cucumber BDD app
Group: Development/Languages
License: MIT
URL: http://cukes.info
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(gherkin)
BuildRequires: rubygem(rspec)
# BuildRequires: rubygem(unindent)
BuildRequires: rubygem(kramdown)
BuildArch: noarch

%description
Core library for the Cucumber BDD app.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
gem build %{gem_name}.gemspec

%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


%check
pushd .%{gem_instdir}
# unindent is not available in Fedora => avoid the requires and expect some
# errors.
grep -Rl unindent spec | xargs sed -i "/require 'unindent'/ s/^/#/"
LANG=C.UTF-8 rspec spec | grep '317 examples, 14 failures'
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE
%{gem_libdir}
# This is not the original file => makes no sense to ship it.
%exclude %{gem_instdir}/cucumber-core.gemspec
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CONTRIBUTING.md
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/HISTORY.md
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/spec

%changelog
* Tue Apr 05 2016 Vít Ondruch <vondruch@redhat.com> - 1.4.0-1
- Initial package
